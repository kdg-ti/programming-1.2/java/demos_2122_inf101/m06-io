package be.kdg.io;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class IODemo {
  public static final String FILE = "film.txt";
  public static final String RESOURCE_FILE = "/filmresources.txt";

  public static void main(String[] args) {
    //   useInputstream();
    //   useResourcestream();
    //   useTryWithResourcesInputstream();
    //  useInputStreamReader();
   // useFileReader();
  //  useBufferedReader();
    useZipWriter();
  }

  private static void useInputstream() {
    InputStream in = null;
    OutputStream out = null;
    try {
      in = new FileInputStream(FILE);
      out = new FileOutputStream("Inputstream_" + FILE);
      int c = in.read();
// you can also construct an input stream from a network url
//      URL url = new URL("http://www.kdg.be");
//      url.openStream();
      while (c != -1) {
        // System.out.println(c);
        out.write(c);
        c = in.read();
      }

    } catch (IOException e) {
      System.err.println("Error reading file " + e);

      //e.printStackTrace();
    } finally {
      try {
        if (in != null) in.close();
        if (out != null) out.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }


  private static void useResourcestream() {
    try {
      IODemo demo = new IODemo();
      InputStream in = demo.getClass().getResourceAsStream(RESOURCE_FILE);
      int c = in.read();
      System.out.println(c);
//      URL url = new URL("http://www.kdg.be");
//      url.openStream();

    } catch (IOException e) {
      System.err.println("Error reading file " + e);
      //e.printStackTrace();
    }
  }

  private static void useTryWithResourcesInputstream() {
    try (
      InputStream in = new FileInputStream(FILE);
      OutputStream out = new FileOutputStream("TryWithResources_" + FILE);
    ) {
      int c = in.read();
// you can also construct an input stream from a network url
//      URL url = new URL("http://www.kdg.be");
//      url.openStream();
      while (c != -1) {
        // System.out.println(c);
        out.write(c);
        c = in.read();
      }

    } catch (IOException e) {
      System.err.println("Error reading file " + e);

      //e.printStackTrace();
    }
  }

  private static void useInputStreamReader() {
    try (
      InputStreamReader in = new InputStreamReader(new FileInputStream(FILE));
      OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(
        "InputStreamReader_" + FILE));
    ) {
      int c = in.read();
      while (c != -1) {
        // System.out.println(c);
        out.write(c);
        c = in.read();
      }

    } catch (IOException e) {
      System.err.println("Error reading file " + e);

      //e.printStackTrace();
    }
  }

  private static void useFileReader() {
    try (
      FileReader in = new FileReader(FILE);
      OutputStreamWriter out = new FileWriter("InputStreamReader_" + FILE);
    ) {
      int c = in.read();
      while (c != -1) {
        // System.out.println(c);
        out.write(c);
        c = in.read();
      }

    } catch (IOException e) {
      System.err.println("Error reading file " + e);

      //e.printStackTrace();
    }
  }

  private static void useBufferedReader() {
    try (
      BufferedReader in = new BufferedReader(new FileReader(FILE));
      BufferedWriter out = new BufferedWriter(new FileWriter("BufferedReader_" + FILE));
    ) {
      String c = in.readLine();
      while (c != null) {
        // System.out.println(c);
        out.write(c + "\n");
        c = in.readLine();
      }

    } catch (IOException e) {
      System.err.println("Error reading file " + e);
    }
  }

  private static void useDataReader() {
    try (
      DataInputStream in = new DataInputStream(new FileInputStream(FILE));
      ZipOutputStream out = new ZipOutputStream(new FileOutputStream("Zipwriter_" + FILE + ".zip"));
    ) {
      out.putNextEntry(new ZipEntry(FILE));
      int c = in.readInt();
      while (c != -1) {
        // System.out.println(c);
        out.write(c );
        c = in.read();
      }
    } catch (IOException e) {
      System.err.println("Error reading file " + e);
      e.printStackTrace();
    }
  }


  private static void useZipWriter() {
    try (
      FileInputStream in = new FileInputStream(FILE);
      ZipOutputStream out = new ZipOutputStream(new FileOutputStream("Zipwriter_" + FILE + ".zip"));
    ) {
      out.putNextEntry(new ZipEntry(FILE));
      int c = in.read();
      while (c != -1) {
        // System.out.println(c);
        out.write(c );
        c = in.read();
      }
    } catch (IOException e) {
      System.err.println("Error reading file " + e);
      e.printStackTrace();
    }
  }
}

